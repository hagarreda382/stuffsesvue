/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,js,tsx}",
],
  theme: {
    extend: {
      // colors:{
      //   "primary-color": "#00668A",
      //   "secondry-color": "#004E71",
      // },
      fontFamily:{
        Roboto:["Roboto, sans-serif"]
      },
      container:{
        padding:"2rem",
        center: true,
      },
      screens:{
        sm:"640px",
        md:"768px"
      }
    },
  },
  plugins: [],
}

