import { createRouter, createWebHistory } from 'vue-router'
import Beranda from '../views/Beranda.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/beranda',
      name: 'beranda',
      component: Beranda
    },
    {
      path: '/shop',
      name: 'shop',
      component: () => import('../views/Shop.vue')
    },
    {
      path: '/blog',
      name: 'blog',
      component: () => import('../views/Blog.vue')
    }
  ]
})

export default router
